Juego creado para TINS 2020 (8 de mayo)

TAM_MAP - Tamaño rectangular del mapa, todo el algoritmo se ajusta a este tamaño.
mapele - Vector de elementos. Es lo que define que contenido debe tener dicha celda en el mapa. Se puede elaborar mas contenido para variar los niveles del juego.

El codigo fuente tiene licencia GPL y los archivos de datos tiene CC (BY-NC-SA).
