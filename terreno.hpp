#ifndef __TERRENO_H__
#define __TERRENO_H__
#include "Base.h"

#define DEPTH 4

class Terreno{
public:
  Terreno();
  ~Terreno();
  int NewTr(int,int);
  void GenGrad();
  void GenTr();
  float Snoise(int,int,int);
  float lerp(float,float,float);
  float perlin(int,int,int);
  float*Grad[DEPTH];
  Uchar*tr;
  int tx,ty,tt;
};

/* Terreno: (byte)
 * 0 - Agua
 * 1 - Arena
 * 2 - Pasto
 * 3 - Arboles
 * 4 - Montaña/Roca
 */

#endif
